%global app                     nginx
%global d_conf                  %{_sysconfdir}/%{app}/conf.d

Name:                           meta-nginx
Version:                        1.0.0
Release:                        3%{?dist}
Summary:                        META-package for install and configure NGINX
License:                        GPLv3

Source10:                       %{app}.custom.conf

Requires:                       nginx

%description
META-package for install and configure NGINX.

# -------------------------------------------------------------------------------------------------------------------- #
# -----------------------------------------------------< SCRIPT >----------------------------------------------------- #
# -------------------------------------------------------------------------------------------------------------------- #

%prep


%install
%{__rm} -rf %{buildroot}

%{__install} -Dp -m 0644 %{SOURCE10} \
  %{buildroot}%{d_conf}/00-%{app}.custom.conf


%files
%config %{d_conf}/00-%{app}.custom.conf


%changelog
* Tue Jul 02 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.0-3
- Update SPEC-file.

* Sat Mar 30 2019 Kitsune Solar <kitsune.solar@gmail.com> - 1.0.0-2
- New version: 1.0.0-2.

* Wed Jan 02 2019 Kitsune Solar <kitsune.solar@gmail.com> - 1.0.0-1
- Initial build.
